import config from './config/user'
import axios from '@/utils/request'

export const reg = (options) => axios.post(config.reg, options);
export const login = (options) => axios.post(config.login, options);
export const validate = () => axios.get(config.validate);

// export const reg = () => {
//     return new Promise((resolve, reject) => {
//         resolve({
//             data: {}
//         })
//     })
// }
// export const login = (option) => {
//     return new Promise((resolve, reject) => {
//         resolve({
//             data: {
//                 token: 'afsdf',
//                 authList: [
//                     { pid: -1, name: '用户管理', id: '1', role: 'aa' },
//                     { pid: 1, name: '用户权限', id: '2', role: 'aa', auth: 'userAuth', path: '/manager/userAuth' },
//                     { pid: 1, name: '用户统计', id: '3', role: 'aa', auth: 'userStatistics', path: '/manager/userStatistics' },
//                     { pid: -1, name: '信息发布', id: '4', role: 'aa', auth: 'infoPublish', path: '/manager/infoPublish' },
//                     { pid: -1, name: '文章管理', id: '5', role: 'aa', auth: 'articleManager', path: '/manager/articleManager' },
//                     { pid: -1, name: '个人中心', id: '6', role: 'bb', auth: 'personal', path: '/manager/personal' },
//                     { pid: -1, name: '我的收藏', id: '7', role: 'bb', auth: 'myCollection', path: '/manager/myCollection' },
//                     { pid: -1, name: '私信消息', id: '8', role: 'bb', auth: 'privateMessage', path: '/manager/privateMessage' },
//                     { pid: -1, name: '我的文章', id: '9', role: 'bb', auth: 'myArticle', path: '/manager/myArticle' }
//                 ]
//             }
//         })
//     })
// }