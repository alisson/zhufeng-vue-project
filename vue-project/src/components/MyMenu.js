import { createNamespacedHelpers } from 'vuex';

let { mapState } = createNamespacedHelpers('user');

export default {
    data() {
        return { list: [] }
    },
    computed: {
        ...mapState(['userInfo'])
    },
    methods: {
        getMenuList(authList) {
            let menu = [];
            let map = {};
            authList.forEach(m => {
                m.children = [];
                map[m.id] = m;
                if (m.pid == -1) {
                    menu.push(m);
                } else {
                    map[m.pid] && map[m.pid].children.push(m);
                }
            });
            return menu;
        }
    },
    mounted() {
        // this.userInfo.authList = [
        //     { pid: -1, name: '用户管理', id: '1', role: 'aa' },
        //     { pid: 1, name: '用户权限', id:  '2', role: 'aa', auth: 'userAuth', path: '/manager/userAuth' },
        //     { pid: 1, name: '用户统计', id:  '3', role: 'aa', auth: 'userStatistics', path: '/manager/userStatistics' },
        //     { pid: -1, name: '信息发布', id: '4', role: 'aa', auth: 'userStatistics', path: '/manager/infoPublish' },
        //     { pid: -1, name: '文章管理', id: '5', role: 'aa', auth: 'userStatistics', path: '/manager/articleManager' },
        //     { pid: -1, name: '个人中心', id: '6', role: 'bb', auth: 'userStatistics', path: '/manager/personal' },
        //     { pid: -1, name: '我的收藏', id: '7', role: 'bb', auth: 'userStatistics', path: '/manager/myCollection' },
        //     { pid: -1, name: '私信消息', id: '8', role: 'bb', auth: 'userStatistics', path: '/manager/privateMessage' },
        //     { pid: -1, name: '我的文章', id: '9', role: 'bb', auth: 'userStatistics', path: '/manager/myArticle' }
        // ]
        this.list = this.getMenuList(this.userInfo.authList);
    },
    render() {
        let renderChildren = (list) => {
            return list.map(child => {
                return child.children.length ?
                    <el-submenu index={child.id}>
                        <div slot="title">{child.name}</div>
                        {/* 如果有孩子的话继续渲染 -- 递归 */}
                        {renderChildren(child.children)}
                    </el-submenu> :
                    <el-menu-item index={child.path}>{child.name}</el-menu-item>
            })
        }
        return <el-menu
            background-color="#333"
            text-color="#fff"
            active-text-color="#ffd04b"
            router={true}
        >
            {renderChildren(this.list)}
        </el-menu>
    }
}