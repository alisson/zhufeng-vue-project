## 学到的知识
#### try catch无法捕获rejected状态的promise的错误
示例：
```javascript
let p = () => {
  return new Promise((resolve, reject) => {
    reject('error')
  })
}

try {
  p()
} catch (e) {
  console.log(7);//不会打印7
}
```
对应项目views/user/Login.vue里的submitForm方法







