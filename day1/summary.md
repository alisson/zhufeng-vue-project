
## 学到的知识
### webpack.require()方法介绍

```javascript
//遍历当前目录下的test文件夹的所有.test.js结尾的文件,不遍历子目录
require.context('./test', false, /.test.js$/);
```
`require.context`函数执行后返回的是一个函数,并且这个函数有3个属性

- resolve {Function} -接受一个参数request,request为test文件夹下面匹配文件的相对路径,返回这个匹配文件相对于整个工程的相对路径
- keys {Function} -返回匹配成功模块的名字组成的数组
- id {String} -执行环境的id,返回的是一个字符串,主要用在module.hot.accept,应该是热加载?
这三个都是作为函数的属性(注意是作为函数的属性,函数也是对象,有对应的属性)
